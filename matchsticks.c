#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<unistd.h>
int howtoplay();
int play();
int Exit();
int players1();
int players2();
int main()
{
    int choice,flag=0;
    do
    {

	printf("\t\t****WELCOME TO THE GAME****\n");
	printf("\n\t\t\t1.HOW TO PLAY");
	printf("\n\t\t\t2.PLAY");
	printf("\n\t\t\t3.EXIT");
	printf("\n\t\t\tEnter a choice: ");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1:
            flag=howtoplay();
			break;
	    case 2:
	    	flag=play();
	    	break;
	    case 3:
	    	flag=Exit();
            break;
	    default:
                system("CLS");
	    		system("COLOR 0C");
	    		printf("\t\t\tINVALID CHOICE");
	    	    break;
	    	    flag=1;
	}
	}while(flag==1);
	return 0;
}

	int howtoplay()
	{

		system("CLS");
		printf("\t\t\tRULES");
		printf("\n1.There are 21 matchsticks.");
		printf("\n2.You have to pick 1, 2, 3 or 4 matchsticks.");
		printf("\n3.Then its computers turn to pick the matchsticks");
		printf("\n4.Whoever is forced to pick up the last matchstick loses the game.");
		char c;
		printf("\nENTER Y TO RETURN TO MAIN MENU: ");
		c=getche();
		if(c=='y' || c=='Y')
        {
            system("CLS");
            return 1;
        }
        else
        {
           system("CLS");
          // system("PAUSE");
           return 0;
        }
	}
	int play()
	{
		int player;
		system("CLS");
		system("COLOR 0B");
		printf("\n1.Single player");
		printf("\n2.2 players");
		printf("\nCHOOSE NUMBER OF PLAYERS: ");
		scanf("%d",&player);
		switch(player)
		{
			case 1:
				players1();
				break;
			case 2:
				players2();
				break;
			default:
				system("CLS");
	    		system("COLOR 0C");
	    		printf("\t\t\tINVALID CHOICE");
	    		break;

		}
	}
	int players1()
	{
	    system("CLS");
	    int user,comp,matchsticks=21;
	    char s[100];
	    printf("ENTER YOUR NAME: ");
	    scanf("%s",&s);
	    system("CLS");
	    system("COLOR 02");
	    printf("\t\t*****THE GAME STARTS*****");
        system("PAUSE");
	  while(matchsticks!=0)
	  {
        system("CLS");
        system("COLOR 0E");
        printf("\n%s enter your number of matchsticks: ",s);
	    scanf("%d",&user);
	    if(user>4||user<1)
	    {
	        system("COLOR 0C");
            printf("YOU CAN PICK ONLY 1 OR 2 OR 3 OR 4 MATCHSTICKS");
	    }
	    else
        {
            matchsticks-=user;
	        comp=5-user;
	        printf("\nComputer's number of matchsticks is: %d\n",comp);
	        matchsticks-=comp;
	        printf("\nTHE REMAINING MATCHSTICKS ARE: %d",matchsticks);
        }

        if(matchsticks==1)
        {
            system("CLS");
            printf("\nTHE REMAINING MATCHSTICKS ARE: %d",matchsticks);
            printf("\n\tYou are the one to pick up the last matchstick");
            system("COLOR 0E");
            printf("\n\t\t**YOU LOST THE GAME**\n");
        }
        system("PAUSE");
      }
      return 0;
    }

    int players2()
    {
        system("CLS");
    	char name1[100],name2[100];
    	int player1,player2,matchsticks=21;
    	printf("\nENTER PLAYER 1 NAME: ");
    	scanf("%s",name1);
    	printf("\nENTER PLAYER 2 NAME: ");
    	scanf("%s",name2);
    	system("CLS");
    	system("COLOR 02");
	    printf("\t\t*****THE GAME STARTS*****");

	    system("PAUSE");
	    while(matchsticks!=0)
	    {
	        system("CLS");
	        xy:
	            system("COLOR 0E");
	    	printf("\n%s enter your number of matchsticks: ",name1);
	        scanf("%d",&player1);
	        if(player1>4||player1<1)
	        {
	            system("COLOR 0C");
                printf("\nYOU CAN PICK ONLY 1 OR 2 OR 3 OR 4 MATCHSTICKS");
                system("PAUSE");
                goto xy;
	        }
	        else
            {
	        matchsticks-=player1;
	        if(matchsticks<=0)
            {
                printf("\n\t%s, you are the one to pick up the last matchstick",name1);
                system("COLOR 09");
                printf("\n\t\t**%s WON THE GAME**\n",name2);
            }
            else
                printf("\nTHE REMAINING MATCHSTICKS ARE: %d\n",matchsticks);
            }
            if(matchsticks>0)
            {
            zy:
                system("COLOR 0A");
	        printf("\n%s enter your number of matchsticks: ",name2);
	        scanf("%d",&player2);
	        if(player2>4||player2<1)
	        {
	            system("COLOR 0C");
                printf("\nYOU CAN PICK ONLY 1 OR 2 OR 3 OR 4 MATCHSTICKS");
                system("PAUSE");
                goto zy;
	        }
	        else
            {
	        matchsticks-=player2;
	        if(matchsticks<=0)
            {
                printf("\n\t%s, you are the one to pick up the last matchstick",name2);
                system("COLOR 09");
                printf("\n\t\t**%s WON THE GAME**\n",name1);
            }
            else
                printf("\nTHE REMAINING MATCHSTICKS ARE: %d\n",matchsticks);
	    	system("PAUSE");
            }
            }
		}
		return 0;

	}

	int Exit()
	{
	    char c;
	    system("CLS");
	    system("COLOR 0A");
	    printf("ARE YOU SURE YOU WANT TO EXIT");
        printf("\nENTER Y OR N: ");
        c=getche();
        if(c=='Y' || c=='y')
        {
            system("CLS");
            system("COLOR 0E");
            printf("\n\t\t\tTHANK YOU\n");
            system("PAUSE");
            return 0;
        }
        else
        {
            system("CLS");
            return 1;
        }
	}

